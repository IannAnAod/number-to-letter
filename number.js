// JavaScript Document
/****************************************************************************

________________________________________________________________________	*
    About 		:	Convertit un nombre entre 0 et 999 en lettre            *
                    D'après le TP1 OpenClassroom Dynamiser vos sites web    *
                    avec JS						                            *
_________________________________________________________________________	*			
	Auteur  	:	LAMBERT Marine		                                    *
	Mail    	:	marine.ldm@gmail.com									*											
	Copyright 	:	mars 2019												*
_________________________________________________________________________	*

*****************************************************************************/

var unitsLetters = ['', 'un', 'deux', 'trois', 'quatre', 'cinq', 'six', 
                    'sept', 'huit', 'neuf', 'dix', 'onze', 'douze', 'treize', 
                    'quatorze', 'quinze', 'seize', 'dix-sept', 'dix-huit', 'dix-neuf'],
    tensLetters = ['', 'dix', 'vingt', 'trente', 'quarante', 'cinquante', 
                    'soixante', 'soixante', 'quatre-vingt', 'quatre-vingt'];


function userNumber (){

    // Action utilisateur
    var number = prompt('Entres un nombre entier entre 0 et 999 :');

    var units = number % 10,
        tens = (number % 100 - units) / 10,
        hundreds = (number % 1000 - number % 100) / 100;

    // Met la chaine de caractère en nombre
    var nb = parseInt(number, 10);

    if (number === null) {
        alert ('OK, à la prochaine !');

    } else if (isNaN(number) === true || number.length === 0) {
        alert ('Ha, ha! Bien tenté, mais tu ne peux entrer qu\'un nombre.');
        userNumber();

    } else if (number.length === 1 || number.length === 2 || number.length === 3) {
        if (nb === 0) {
            alert('zéro');
        } else {
            // Traitement des unités
            unitsOut = (units === 1 && tens > 0 && tens !== 8 ? 'et-' : '') + unitsLetters[units];

            // Traitement des dizaines
            if (tens === 1 && units > 0) {
                tensOut = unitsLetters[10 + units];
                unitsOut = '';

            } else if (tens === 7 || tens === 9) {
                tensOut = tensLetters[tens] + '-' + (tens === 7 && units === 1 ? 'et-' : '') + unitsLetters[10 + units];
                unitsOut = '';

            } else {
                tensOut = tensLetters[tens];
            }

            tensOut += (units === 0 && tens === 8 ? 's' : '');

            // Traitement des centaines
            hundredsOut = (hundreds > 1 ? unitsLetters[hundreds] + '-' : '') + (hundreds > 0 ? 'cent' : '') + (hundreds > 1 && tens == 0 && units == 0 ? 's' : '');

            // Retour du total
            alert (hundredsOut + (hundredsOut && tensOut ? '-' : '') + tensOut + (hundredsOut && unitsOut || tensOut && unitsOut ? '-' : '') + unitsOut);
            userNumber();
        }
    
    } else if (number.length > 2) {
        alert ('Tu as entré un nombre trop grand !');
        userNumber();

    } else {
        alert ('Mais qu\'est-ce qui te passes par la tête !?!?');
        userNumber();
    }
}

// userNumber();

var btn = document.querySelector('input');
btn.addEventListener('click', userNumber);
